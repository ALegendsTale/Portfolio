var White = 255;
var x = false;

window.onscroll = function() {setBrightness()};

function setBrightness() {
    var scrollPercent = window.scrollY / (document.body.offsetHeight - window.innerHeight);
    var a = White * easeInCubic(scrollPercent, 0, scrollPercent, 1);

    setTextBrightness(a);
}

function setTextBrightness(a){
    document.documentElement.style.setProperty('--text-brightness', 'rgba('+a+','+a+","+a+","+1);
}

/*
function hideFooter(){
    var elements = document.getElementsByClassName("footer-section");
    for(var i=0; i < elements.length; i++)
    {
        elements[i].textContent = "";
    }
}
*/